<?php

namespace Tmpl\Template;

use Tmpl\Environment;

class TemplateWrapper {
    private $env;
    private $template;

    public function __construct(Environment $env, Template $template) {
        $this->env = $env;
        $this->template = $template;
    }

    public function render($vars = []) {
        $this->template->render($vars);
    }

    public function compute($vars = []) {
        $level = ob_get_level();
        ob_start();
        try {
            $this->render($vars);
        } catch (\Throwable $e) {
            while (ob_get_level() > $level) {
                ob_end_clean();
            }

            throw $e;
        }

        return ob_get_clean();
    }
}