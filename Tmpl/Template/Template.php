<?php

namespace Tmpl\Template;

abstract class Template {
    protected $env;

    public function __construct($env) {
        $this->env = $env;
    }

    public abstract function render($vars);
}
