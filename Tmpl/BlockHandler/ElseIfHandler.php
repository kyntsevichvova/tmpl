<?php

namespace Tmpl\BlockHandler;

use \Tmpl\Token\TokenStream;

class ElseIfHandler extends AbstractHandler {
    public function parse(TokenStream $stream) {
        $expr = $this->env->getExpressionParser()->parseExpression($stream);
        return new \Tmpl\Node\ElseIfNode($expr);
    }
}