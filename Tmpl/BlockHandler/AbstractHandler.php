<?php

namespace Tmpl\BlockHandler;

use \Tmpl\Token\TokenStream;

abstract class AbstractHandler {
    protected $env;

    public function __construct($env) {
        $this->env = $env;
    }

    public abstract function parse(TokenStream $stream);
}