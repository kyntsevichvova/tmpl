<?php

namespace Tmpl\BlockHandler;

use \Tmpl\Token\Token;
use \Tmpl\Token\TokenStream;

class ForHandler extends AbstractHandler {
    public function parse(TokenStream $stream) {
        $expr1 = $this->env->getExpressionParser()->parseAssignmentExpression($stream);

        $in = $stream->expect(Token::NAME, 'in');
        
        $expr2 = $this->env->getExpressionParser()->parseExpression($stream);

        return new \Tmpl\Node\ForNode($expr1, $expr2);
    }
}