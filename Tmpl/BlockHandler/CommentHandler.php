<?php

namespace Tmpl\BlockHandler;

use \Tmpl\Token\Token;
use \Tmpl\Token\TokenStream;

class CommentHandler extends AbstractHandler {
    public function parse(TokenStream $stream) {     
        $comment = "";
        while ($stream->peek()->getType() !== Token::COMMENT_END) {
            $comment .= $stream->pop()->getValue();
        }
        return new \Tmpl\Node\CommentNode($comment);
    }
}