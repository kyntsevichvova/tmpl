<?php

namespace Tmpl\BlockHandler;

use \Tmpl\Token\TokenStream;

class IncludeHandler extends AbstractHandler {
    public function parse(TokenStream $stream) {
        $templateName = $this->env->getExpressionParser()->parse($stream);
        return new \Tmpl\Node\IncludeNode($templateName);
    }
}