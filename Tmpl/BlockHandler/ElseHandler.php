<?php

namespace Tmpl\BlockHandler;

use \Tmpl\Token\TokenStream;

class ElseHandler extends AbstractHandler {
    public function parse(TokenStream $stream) {
        return new \Tmpl\Node\ElseNode();
    }
}
