<?php

namespace Tmpl\BlockHandler;

use \Tmpl\Token\TokenStream;

class OutputHandler extends AbstractHandler {
    public function parse(TokenStream $stream) {
        $node = new \Tmpl\Node\OutputNode($this->env->getExpressionParser()->parseExpression($stream));
        return $node;
    }
}