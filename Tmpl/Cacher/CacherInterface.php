<?php

namespace Tmpl\Cacher;

interface CacherInterface {
    public function load($cacheKey);
    public function write($cacheKey, $content);
    public function getCacheKey($templateName, $className);
    public function getTimestamp($cacheKey);
}