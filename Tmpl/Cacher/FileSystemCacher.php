<?php

namespace Tmpl\Cacher;

class FileSystemCacher implements CacherInterface {
    private $cacheDirectory;

    public function __construct($cacheDirectory) {
        $this->cacheDirectory = rtrim($cacheDirectory, '/') . '/';
    }

    public function load($cacheKey) {
        if (file_exists($cacheKey)) {
            include_once $cacheKey;
        }
    }

    public function write($cacheKey, $content) {
        $dir = dirname($cacheKey);
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        } elseif (!is_writeable($dir)) {
            // Error
        }

        $tmpFile = tempnam($dir, basename($cacheKey));
        if (file_put_contents($tmpFile, $content) !== false && rename($tmpFile, $cacheKey)) {
            // invalidate?
            return;
        }
    }

    public function getCacheKey($templateName, $className) {
        $hash = hash('sha256', $className);

        return $this->cacheDirectory . $hash . '.php';
    }

    public function getTimestamp($cacheKey) {
        if (!file_exists($cacheKey)) {
            return 0;
        }

        return (int) filemtime($cacheKey);
    }
}