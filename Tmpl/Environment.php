<?php

namespace Tmpl;

use \Tmpl\Cacher\CacherInterface;
use \Tmpl\Loader\LoaderInterface;
use \Tmpl\Node\AbstractNode;
use \Tmpl\Template\TemplateWrapper;
use \Tmpl\Token\TokenStream;

class Environment {
    private $core;

    private $loader;
    private $cacher;

    private $lexer;
    private $parser;
    private $compiler;
    private $expressionParser;

    private $templateClassPrefix;
    private $loadedTemplates;

    private $handlers;
    private $functions;
    private $filters;
    private $unary;
    private $binary;


    public function __construct(LoaderInterface $loader, CacherInterface $cacher, $options = []) {
        $this->core = new Core();
        $this->loader = $loader;
        $this->cacher = $cacher;

        $this->templateClassPrefix = "__template_";

        $this->expressionParser = new \Tmpl\ExpressionParser($this);

        //$this->handlers = $this->core->getBlockHandlers();
        $this->handlers = [
            'for'     => new \Tmpl\BlockHandler\ForHandler($this),
            'if'      => new \Tmpl\BlockHandler\IfHandler($this),
            'elseif'  => new \Tmpl\BlockHandler\ElseIfHandler($this),
            'else'    => new \Tmpl\BlockHandler\ElseHandler($this),
            'output'  => new \Tmpl\BlockHandler\OutputHandler($this),
            'include' => new \Tmpl\BlockHandler\IncludeHandler($this),
            'comment' => new \Tmpl\BlockHandler\CommentHandler($this)
        ];
        $this->functions = $this->core->getFunctions();
        $this->filters = $this->core->getFilters();
        $this->unary = $this->core->getUnaryOperators();
        $this->binary = $this->core->getBinaryOperators();
    }

    public function load($templateName) {
        if ($templateName instanceof TemplateWrapper) {
            return $templateName;
        }

        return new TemplateWrapper($this, $this->loadTemplate($templateName));
    }

    private function loadTemplate($templateName) {
        return $this->loadClass($this->getTemplateClass($templateName), $templateName);
    }

    private function loadClass($className, $templateName) {
        if (isset($this->loadedTemplates[$className])) {
            return $this->loadedTemplates[$className];
        }

        if (!class_exists($className, false)) {
            $cacheKey = $this->cacher->getCacheKey($templateName, $className);

            if ($this->isFresh($this->loader->getTimestamp($templateName), $this->cacher->getTimestamp($cacheKey))) {
                $this->cacher->load($cacheKey);
            }

            if (!class_exists($className, false)) {
                $source = $this->loader->loadSource($templateName);
                $compiled = $this->compileSource($source, $className);

                $this->cacher->write($cacheKey, $compiled);
                $this->cacher->load($cacheKey);
    
                if (!class_exists($className, false)) {
                    error_log("Unable to load $className from cache with key $cacheKey");
                    // Unable to load from cache
                    // Error
                }
            }
        }

        try {
            $this->loadedTemplates[$className] = new $className($this);
        } finally {

        }

        return $this->loadedTemplates[$className];
    }

    public function getTemplateClass($templateName) {
        $key = $this->loader->getCacheKey($templateName); // . $this->optionsHash /* for different options */

        return $this->templateClassPrefix . hash("sha256", $key);
    }

    public function isFresh($loadedTimestamp, $cachedTimestamp) {
        return $loadedTimestamp <= $cachedTimestamp;
    }

    public function compileSource($source, $className) {
        try {
            return $this->compile($this->parse($this->tokenize($source), $className));
        } catch(\Exception $e) {

        }
    }

    public function tokenize(Source $source) {
        if (is_null($this->lexer)) {
            $this->lexer = new Lexer($this);
        }

        return $this->lexer->tokenize($source);
    }

    public function parse(TokenStream $stream, $className) {
        if (is_null($this->parser)) {
            $this->parser = new Parser($this);
        }

        return $this->parser->parse($stream, $className);
    }

    public function compile(AbstractNode $node) {
        if (is_null($this->compiler)) {
            $this->compiler = new Compiler($this);
        }

        return $this->compiler->compile($node)->getSource();
    }

    public function getAttribute($object, $attribute) {
        if (is_null($object)) {
            // Error
        }

        if (is_array($object)) {
            if (!array_key_exists($attribute, $object)) {
                // Error
            }
            return $object[$attribute];
        } elseif (is_object($object)) {
            if (isset($object->$attribute) || array_key_exists((string) $attribute, $object)) {
                return $object->$attribute;
            } else {
                // Error
            }
        } else {
            // Error
        }
    }

    public function getHandlers() {
        return $this->handlers;
    }

    public function getFunctions() {
        return $this->functions;
    }

    public function getFilters() {
        return $this->filters;
    }

    public function getUnary() {
        return $this->unary;
    }

    public function getBinary() {
        return $this->binary;
    }

    public function getExpressionParser() {
        return $this->expressionParser;
    }
}