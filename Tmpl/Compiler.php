<?php

namespace Tmpl;

use \Tmpl\Node\AbstractNode;

class Compiler {
    private $env;

    private $indentation;
    private $source;

    public function __construct($env) {
        $this->env = $env;
    }

    public function getSource() {
        return $this->source;
    }

    public function compile(AbstractNode $node) {
        $this->source = '';

        $this->indentation = 0;

        $node->compile($this);

        return $this;        
    }

    public function subcompile(AbstractNode $node) {
        $node->compile($this);

        return $this;
    }

    public function raw($string) {
        $this->source .= $string;

        return $this;
    }

    public function write($string) {
        $this->source .= str_repeat(' ', $this->indentation * 4) . $string;

        return $this;
    }

    public function string($value) {
        $this->source .= sprintf('"%s"', addcslashes($value, "\0\t\"\$\\"));

        return $this;
    }

    public function repr($value) {
        if (\is_int($value) || \is_float($value)) {
            if (false !== $locale = setlocale(LC_NUMERIC, '0')) {
                setlocale(LC_NUMERIC, 'C');
            }

            $this->raw(var_export($value, true));

            if (false !== $locale) {
                setlocale(LC_NUMERIC, $locale);
            }
        } elseif (\is_null($value)) {
            $this->raw('null');
        } elseif (\is_bool($value)) {
            $this->raw($value ? 'true' : 'false');
        } elseif (\is_array($value)) {
            $this->raw('array(');
            $first = true;
            foreach ($value as $key => $v) {
                if (!$first) {
                    $this->raw(', ');
                }
                $first = false;
                $this->repr($key);
                $this->raw(' => ');
                $this->repr($v);
            }
            $this->raw(')');
        } else {
            $this->string(trim($value, "'"));
        }

        return $this;
    }

    public function indent($step = 1) {
        $this->indentation += $step;
        
        return $this;
    }
    
    public function outdent($step = 1) {
        if ($this->indentation < $step) {
            // Error
        } else {
            $this->indentation -= $step;
        }
        
    
        return $this;
    }
}
