<?php

namespace Tmpl;

use \Tmpl\Token\Token;
use \Tmpl\Token\TokenStream;

class Parser {
    private $env;
    private $handlers;

    private $nodeStack;
    private $node;

    public function __construct($env) {
        $this->env = $env;
        $this->handlers = $env->getHandlers();
    }

    public function parse(TokenStream $stream, $className) {
        $this->node = new \Tmpl\Node\BodyNode($className);
        $this->nodeStack = [];

        while (!$stream->empty()) {
            $token = $stream->peek();
            switch ($token->getType()) {
                case Token::DATA:
                    $this->node->addChild(new \Tmpl\Node\DataNode($token->getValue()));
                    $stream->pop();
                    break;
                
                case Token::COMMENT_START:
                    $stream->expect(Token::COMMENT_START);
                    $this->node->addChild($this->handlers['comment']->parse($stream));
                    $stream->expect(Token::COMMENT_END);
                    break;
                
                case Token::OUTPUT_START:
                    $stream->expect(Token::OUTPUT_START);
                    $this->node->addChild($this->handlers['output']->parse($stream));
                    $stream->expect(Token::OUTPUT_END);
                    break;
                
                case Token::BLOCK_START:
                    $tempNode = $this->parseBlock($stream);
                    if ($tempNode instanceof \Tmpl\Node\EndNode) {
                        $this->closeNode($tempNode);
                    } elseif ($tempNode instanceof \Tmpl\Node\ElseNode) {
                        if ($this->node instanceof \Tmpl\Node\ElseableNode) {
                            $this->node->setElse($tempNode);
                            if ($tempNode->isNestedNode()) {
                                $this->pushNode($tempNode);
                            }
                        } else {
                            // Error
                        }
                    } else {
                        $this->node->addChild($tempNode);
                        if ($tempNode->isNestedNode()) {
                            $this->pushNode($tempNode);
                        }
                    }
                    break;
                
                case Token::WHITESPACE:
                    $stream->skip(Token::WHITESPACE);
                    break;
                
                default: 
                    // Error
            }
        }

        return $this->node;
    }

    public function parseBlock(TokenStream $stream) {
        $stream->expect(Token::BLOCK_START);
        
        $token = $stream->expect(Token::NAME);
        $blockName = $token->getValue();

        
        if (isset($this->handlers[$blockName])) {
            $node = $this->handlers[$blockName]->parse($stream);
        } elseif (strncmp($blockName, "end", 3) === 0) {
            $node = new \Tmpl\Node\EndNode($blockName);
        } else {
            // Error
        }
        $stream->expect(Token::BLOCK_END);

        return $node;
    }

    public function closeNode(\Tmpl\Node\EndNode $endNode) {
        $size = count($this->nodeStack);
        $endName = $endNode->getAttribute('endName');
        while ($size > 0) {
            if ($this->node->isFullClose($endName)) {
                $this->popNode();
                $size--;
                break;
            } elseif ($this->node->isPartialClose($endName)) {
                $this->popNode();
                $size--;
            } else {
                // Error
            }
        }
    }

    private function pushNode(\Tmpl\Node\AbstractNode $node) {
        $this->nodeStack[] = $this->node;
        $this->node = $node;
    }

    private function popNode() {
        if (count($this->nodeStack) === 0) {
            // Error
        }

        $this->node = array_pop($this->nodeStack);
    }
}