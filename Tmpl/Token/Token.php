<?php

namespace Tmpl\Token;

class Token {
    const DATA          = 0;
    const COMMENT_START = 1;
    const COMMENT_END   = 2;
    const OUTPUT_START  = 3;
    const OUTPUT_END    = 4;
    const BLOCK_START   = 5;
    const BLOCK_END     = 6;
    const WHITESPACE    = 7;
    const OPERATOR      = 8;
    const NUMBER_LIT    = 9;
    const STRING_LIT    = 10;
    const PUNCTUATION   = 11;
    const NAME          = 12;

    private $type;
    private $value;

    public function __construct($type, $value) {
        $this->type = $type;
        $this->value = $value;
        //echo "TOKEN: " . $type . " " . $value . "<br>";
    }

    public function getType() {
        return $this->type;
    }

    public function getValue() {
        return $this->value;
    }

    public function test($type, $value = null) {
        if (is_null($value)) {
            return $this->type == $type;
        } else {
            return $this->type == $type && $this->value == $value;
        }
    }
}