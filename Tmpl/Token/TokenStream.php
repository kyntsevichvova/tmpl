<?php 

namespace Tmpl\Token;

class TokenStream {
    private $tokens;
    private $cursor;
    private $size;
    
    public function __construct() {
        $this->cursor = 0;
    }

    public function empty() {
        return $this->cursor >= $this->size;
    }

    public function push(Token $token) {
        $this->tokens[] = $token;
        $this->size++;
    }

    public function peek() {
        if ($this->cursor < count($this->tokens)) {
            return $this->tokens[$this->cursor];
        } else {
            return null;
        }
    }

    public function pop() {
        if ($this->cursor < count($this->tokens)) {
            return $this->tokens[$this->cursor++];
        } else {
            return null;
        }
    }

    public function expect($tokenType, $tokenValue = null) {
        if ($this->empty() || !$this->peek()->test($tokenType, $tokenValue)) {
            // Error
            throw new \Exception("Expectation failed");
        }

        return $this->pop();
    }

    public function skip($tokenType) {
        while (!$this->empty() && $this->peek()->getType() == $tokenType) {
            $this->pop();
        }
    }
}