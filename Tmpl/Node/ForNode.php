<?php

namespace Tmpl\Node;

use \Tmpl\Compiler;

class ForNode extends ElseableNode {
    private $expr1;
    private $expr2;

    public function __construct($expr1, $expr2) {
        $this->expr1 = $expr1;
        $this->expr2 = $expr2;
    }

    public function compile(Compiler $compiler) {
        $compiler
            ->write("foreach (")
            ->subcompile($this->expr2)
            ->raw(" as ")
            ->subcompile($this->expr1)
            ->raw(") {\n")
            ->indent();
        
        foreach ($this->childNodes as $node) {
            $compiler->subcompile($node);
        }

        $compiler
            ->outdent()
            ->write("}\n");

        /*if (!is_null($this->elseNode)) {
            
        }*/
    }

    /*
        ForNode can only be closed by "endfor"
    */
    public function isFullClose($value) {
        return $value === "endfor";
    }
    
    public function isPartialClose($value) {
        return false;
    }
}