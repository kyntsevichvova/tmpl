<?php

namespace Tmpl\Node;

use \Tmpl\Compiler;

class EndNode extends AbstractNode {
    public function __construct($endName) {
        parent::__construct([], ['endName' => $endName]);
    }

    /*
        EndNode is temporal and exists for a moment,
        so it shouldn't be compiled
    */
    public function compile(Compiler $compiler) {
        // Error
    }

    /*
        EndNode is temporal, so such checks shouldn't happen
    */
    public function isFullClose($value) {
        // Error
    }

    public function isPartialClose($value) {
        // Error
    }
}