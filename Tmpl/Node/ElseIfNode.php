<?php

namespace Tmpl\Node;

use \Tmpl\Compiler;

class ElseIfNode extends ElseableNode {
    private $expr;

    public function __construct($expr) {
        $this->expr = $expr;
    }

    public function compile(Compiler $compiler) {
        $compiler
            ->write('elseif (')
            ->subcompile($this->expr)
            ->write(") {\n")
            ->indent();
        
        foreach ($this->childNodes as $node) {
            $compiler->subcompile($node);
        }
        
        $compiler
            ->outdent()
            ->write("}\n");
        
        if (!is_null($this->elseNode)) {
            $compiler
                ->raw(' ')
                ->subcompile($this->elseNode);
        }
    }

    /*
        ElseIfNode doesn't need to be closed,
        but it won't stop parent If block being closed by "endif"
    */
    public function isFullClose($value) {
        return false;
    }
    
    public function isPartialClose($value) {
        return $value == "endif";
    }
}