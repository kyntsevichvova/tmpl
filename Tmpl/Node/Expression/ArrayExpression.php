<?php

namespace Tmpl\Node\Expression;

use Tmpl\Compiler;

class ArrayExpression extends AbstractExpression {
    /*public function __construct() {

    }*/

    public function compile(Compiler $compiler) {
        $compiler->raw('[');
        $first = true;
        foreach ($this->childNodes as $node) {
            if (!$first) {
                $compiler->raw(', ');
            } else {
                $first = false;
            }
            $compiler
                ->subcompile($node);
            // key => value?

        }
        $compiler->raw(']');
    }
}