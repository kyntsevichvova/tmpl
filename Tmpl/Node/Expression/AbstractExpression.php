<?php

namespace Tmpl\Node\Expression;

abstract class AbstractExpression extends \Tmpl\Node\AbstractNode {
    public function isFullClose($value) {
        return false;
    }

    public function isPartialClose($value) {
        return true;
    }
}