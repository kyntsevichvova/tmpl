<?php

namespace Tmpl\Node\Expression;

use \Tmpl\Compiler;

class NameExpression extends AbstractExpression {
    public function __construct($name) {
        parent::__construct([], ['name' => $name]);
    }

    public function compile(Compiler $compiler) {
        $name = $this->getAttribute('name');
        $compiler
            ->raw('(isset($vars[')
            ->string($name)
            ->raw(']) || array_key_exists(')
            ->string($name)
            ->raw(', $vars) ? $vars[')
            ->string($name)
            ->raw('] : null')
            ->raw(')');
        // Probably throw error
    }
}