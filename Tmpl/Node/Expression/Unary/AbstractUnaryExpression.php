<?php

namespace Tmpl\Node\Expression\Binary;

use \Tmpl\Compiler;

abstract class AbstractUnaryExpression extends \Tmpl\Node\Expression\AbstractExpression {
    protected $node;

    public function __construct($node) {
        $this->node = $node;
    }

    public function compile(Compiler $compiler) {
        $compiler->raw(' ');
        $this->operator($compiler);
        $compiler->subcompile($this->node);
    }

    public abstract function operator(Compiler $compiler);
}