<?php

namespace Tmpl\Node\Expression\Unary;

use Tmpl\Compiler;

class MinusUnary extends AbstractUnaryExpression {
    public function operator(Compiler $compiler) {
        return $compiler->raw('-');
    }
}