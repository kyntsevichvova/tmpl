<?php

namespace Tmpl\Node\Expression;

use Tmpl\Compiler;

class FilterExpression extends AbstractExpression {
    public function __construct($name, $node, $args) {
        parent::__construct(['node' => $node, 'args' => $args], ['name' => name]);
    }

    public function compile(Compiler $compiler) {
        $compiler
            ->raw('(')
            ->raw('\Tmpl\FilterWrapper::')
            ->raw($this->getAttribute('name'))
            ->raw('(')
            ->subcompile($this->childNodes['node'])
            ->raw(', ')
            ->subcompile($this->childNodes['args'])
            ->raw(')');
    }
}