<?php

namespace Tmpl\Node\Expression;

use Tmpl\Compiler;

class ConstantExpression extends AbstractExpression {
    public function __construct($value) {
        parent::__construct([], ['value' => $value]);
    }

    public function compile(Compiler $compiler) {
        $compiler->repr($this->getAttribute('value'));
    }
}