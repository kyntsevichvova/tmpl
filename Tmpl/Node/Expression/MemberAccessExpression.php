<?php

namespace Tmpl\Node\Expression;

use Tmpl\Compiler;

class MemberAccessExpression extends AbstractExpression {
    public function __construct($node, $member) {
        parent::__construct(['node' => $node, 'member' => $member], []);
    }

    public function compile(Compiler $compiler) {
        $compiler
            ->raw('($this->env->getAttribute(')
            ->subcompile($this->childNodes['node'])
            ->raw(', ')
            ->subcompile($this->childNodes['member'])
            ->raw('))');
    }
}