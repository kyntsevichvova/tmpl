<?php

namespace Tmpl\Node\Expression;

use \Tmpl\Compiler;

class AssignNameExpression extends NameExpression {
    public function compile(Compiler $compiler) {
        $compiler
            ->raw('$vars[')
            ->string($this->getAttribute('name'))
            ->raw(']');
    }
}