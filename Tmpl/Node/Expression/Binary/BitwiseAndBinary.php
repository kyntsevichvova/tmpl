<?php

namespace Tmpl\Node\Expression\Binary;

use Tmpl\Compiler;

class BitwiseAndBinary extends AbstractBinaryExpression {
    public function operator(Compiler $compiler) {
        return $compiler->raw('&');
    }
}