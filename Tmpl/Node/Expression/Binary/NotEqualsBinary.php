<?php

namespace Tmpl\Node\Expression\Binary;

use Tmpl\Compiler;

class NotEqualsBinary extends AbstractBinaryExpression {
    public function operator(Compiler $compiler) {
        return $compiler->raw('!=');
    }
}