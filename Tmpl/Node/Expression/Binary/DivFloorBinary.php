<?php

namespace Tmpl\Node\Expression\Binary;

use Tmpl\Compiler;

class DivFloorBinary extends AbstractBinaryExpression {
    public function compile(Compiler $compiler) {
        $compiler->raw('(int) floor(');
        parent::compile($compiler);
        $compiler->raw(')');
    }

    public function operator(Compiler $compiler) {
        return $compiler->raw('/');
    }
}