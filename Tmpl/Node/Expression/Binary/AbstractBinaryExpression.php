<?php

namespace Tmpl\Node\Expression\Binary;

use \Tmpl\Compiler;

abstract class AbstractBinaryExpression extends \Tmpl\Node\Expression\AbstractExpression {
    protected $left;
    protected $right;

    public function __construct($left, $right) {
        $this->left = $left;
        $this->right = $right;
    }

    public function compile(Compiler $compiler) {
        $compiler
            ->raw('(')
            ->subcompile($this->left)
            ->raw(' ');

        $this->operator($compiler);

        $compiler
            ->raw(' ')
            ->subcompile($this->right)
            ->raw(')');
    }

    public abstract function operator(Compiler $compiler);
}