<?php

namespace Tmpl\Node\Expression;

use Tmpl\Compiler;

class FunctionExpression extends AbstractExpression {
    public function __construct($name, $args) {
        parent::__construct(['args' => $args], ['name' => $name]);
    }

    public function compile(Compiler $compiler) {
        $compiler
            ->raw($this->getAttribute('name'))
            ->raw('(')
            ->subcompile($this->childNodes['args'])
            ->raw(')');
    }
}