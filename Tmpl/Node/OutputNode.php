<?php

namespace Tmpl\Node;

use \Tmpl\Compiler;

class OutputNode extends AbstractNode {

    public function __construct($expr) {
        parent::__construct(['expr' => $expr]);
    }

    public function compile(Compiler $compiler) {
        $compiler
            ->write("echo ")
            ->subcompile($this->childNodes['expr'])
            ->raw(";\n");
    }

    public function isNestedNode() {
        return false;
    }

    /*
        OutputNode doesn't need to be closed,
        but it won't stop parent blocks being closed
    */
    public function isFullClose($value) {
        return false;
    }

    public function isPartialClose($value) {
        return true;
    }
}