<?php

namespace Tmpl\Node;

use \Tmpl\Compiler;

class DataNode extends AbstractNode {

    public function __construct($data) {
        parent::__construct([], ['data' => $data]);
    }

    public function compile(Compiler $compiler) {
        $compiler
            ->write("echo ")
            ->string($this->getAttribute("data"))
            ->raw(";\n")
        ;
    }

    public function isNestedNode() {
        return false;
    }

    /*
        DataNode doesn't need to be closed,
        but it won't stop parent blocks being closed
    */
    public function isFullClose($value) {
        return false;
    }
    public function isPartialClose($value) {
        return true;
    }
}