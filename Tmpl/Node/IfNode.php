<?php

namespace Tmpl\Node;

use \Tmpl\Compiler;

class IfNode extends ElseableNode {
    private $expr;

    public function __construct($expr) {
        $this->expr = $expr;
    }

    public function compile(Compiler $compiler) {
        $compiler
            ->write('if (')
            ->subcompile($this->expr)
            ->write(") {\n")
            ->indent();
        
        foreach ($this->childNodes as $node) {
            $compiler->subcompile($node);
        }
        
        $compiler
            ->outdent()
            ->write("}\n");
        
        if (!is_null($this->elseNode)) {
            $compiler
                ->raw(' ')
                ->subcompile($this->elseNode);
        }
    }

    /*
        IfNode can only be closed by "endif"
    */
    public function isFullClose($value) {
        return $value === "endif";
    }
    
    public function isPartialClose($value) {
        return false;
    }
}