<?php

namespace Tmpl\Node;

abstract class ElseableNode extends AbstractNode {
    protected $elseNode = null;
    
    public function setElse($node) {
        $this->elseNode = $node;
    }
}