<?php

namespace Tmpl\Node;

use \Tmpl\Compiler;

class CommentNode extends AbstractNode {
    public function __construct($comment) {
        parent::__construct([], ['comment' => $comment]);
    }

    public function compile(Compiler $compiler) {
    
    }

    public function isNestedNode() {
        return false;
    }

    /*
        CommentNode doesn't need to be closed,
        but it won't stop parent blocks being closed
    */
    public function isFullClose($value) {
        return false;
    }

    public function isPartialClose($value) {
        return true;
    }
}