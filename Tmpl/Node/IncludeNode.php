<?php

namespace Tmpl\Node;

use \Tmpl\Compiler;

class IncludeNode extends AbstractNode {
    public function __construct($templateName) {
        parent::__construct(['inclusionTemplate' => $templateName]);
    }

    public function compile(Compiler $compiler) {
        $compiler
            ->write('$template = $this->env->load(')
            ->subcompile($this->childNodes['inclusionTemplate'])
            ->raw(");\n")
            ->write('$template->render($vars);')
            ->raw("\n");
    }

    public function isNestedNode() {
        return false;
    }

    /*
        IncludeNode doesn't need to be closed,
        but it won't stop parent blocks being closed
    */
    public function isFullClose($value) {
        return false;
    }

    public function isPartialClose($value) {
        return true;
    }
}