<?php

namespace Tmpl\Node;

use \Tmpl\Compiler;

abstract class AbstractNode {
    protected $attributes;
    protected $childNodes;
    protected $parentNode;

    public function __construct(array $nodes = [], array $attributes = []) {
        $this->childNodes = $nodes;
        $this->attributes = $attributes;
    }

    public function addChild(AbstractNode $node) {
        $this->childNodes[] = $node;
    }

    public function getAttribute($attr) {
        return $this->attributes[$attr];
    }

    public function setAttribute($attr, $value) {
        $this->attributes[$attr] = $value;
    }

    public function isNestedNode() {
        return true;
    }

    public abstract function isFullClose($value);
    public abstract function isPartialClose($value);

    public abstract function compile(Compiler $compiler);
}