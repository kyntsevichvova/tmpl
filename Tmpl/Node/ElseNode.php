<?php

namespace Tmpl\Node;

use \Tmpl\Compiler;

class ElseNode extends AbstractNode {
    public function compile(Compiler $compiler) {
        $compiler
            ->write("else {\n");
        
        foreach ($this->childNodes as $node) {
            $compiler->subcompile($node);
        }
        
        $compiler
            ->outdent()
            ->write("}\n");
    }

    /*
        ElseNode doesn't need to be closed,
        but it won't stop parent If block being closed by "endif"
    */
    public function isFullClose($value) {
        return false;
    }
    
    public function isPartialClose($value) {
        return $value == "endif";
    }
}