<?php

namespace Tmpl\Node;

use \Tmpl\Compiler;

class BodyNode extends AbstractNode {

    public function __construct($className) {
        parent::__construct([], ['className' => $className]);
    }

    public function compile(Compiler $compiler) {
        $compiler
            ->write("<?php\n\n")
            ->write("class ")
            ->raw($this->getAttribute('className'))
            ->raw(" extends \Tmpl\Template\Template {\n")
            ->indent()
            ->write("public function render(\$vars) {\n")
            ->indent();
        
        foreach ($this->childNodes as $node) {
            $compiler->subcompile($node);
        }

        $compiler
            ->outdent()->write("}\n")
            ->outdent()->write("}\n");
    }

    /*
        Can be fully and partially closed by anything,
        probably would be changed later,
        as BodyNode should be the only root node
    */
    public function isFullClose($value) {
        return true;
    }

    public function isPartialClose($value) {
        return true;
    }
}