<?php

namespace Tmpl\Loader;

use Tmpl\Source;

class FileSystemLoader implements LoaderInterface {
    private $templatesRoot;
    private $cachedPaths;

    public function __construct($path) {
        $this->templatesRoot = $path;
    }

    public function loadSource($templateName) {
        $path = $this->findTemplate($templateName);

        return new Source(file_get_contents($path), $templateName, $path);
    }

    public function getCacheKey($templateName) {
        return $this->findTemplate($templateName);
    }

    public function getTimestamp($templateName) {
        return filemtime($this->findTemplate($templateName));
    }

    private function findTemplate($templateName) {
        if (isset($this->cachedPaths[$templateName])) {
            return $this->cachedPaths[$templateName];
        }

        $path = $this->templatesRoot . '/' . $templateName;

        if (is_file($path)) {
            $this->cachedPaths[$templateName] = $path;
            return $path; 
        }

        // Error, no such file
    }
}