<?php

namespace Tmpl\Loader;

interface LoaderInterface {
    public function loadSource($templateName);
    public function getCacheKey($templateName);
    public function getTimestamp($templateName);
}