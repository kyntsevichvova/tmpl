<?php

namespace Tmpl;

class Core {
    public function getBlockHandlers() {
        return 
        [
            'for'     => new \Tmpl\BlockHandler\ForHandler($this),
            'if'      => new \Tmpl\BlockHandler\IfHandler($this),
            'elseif'  => new \Tmpl\BlockHandler\ElseIfHandler($this),
            'else'    => new \Tmpl\BlockHandler\ElseHandler($this),
            'output'  => new \Tmpl\BlockHandler\OutputHandler($this),
            'include' => new \Tmpl\BlockHandler\IncludeHandler($this),
            'comment' => new \Tmpl\BlockHandler\CommentHandler($this)
        ];
    }

    public function getFunctions() {
        return [];
    }

    public function getFilters() {
        return [];
    }

    public function getUnaryOperators() {
        return 
        [
            '!' => ['precedence' => 500, 'class' => '\Tmpl\Node\Expression\Unary\NotUnary'],
            '-' => ['precedence' => 500, 'class' => '\Tmpl\Node\Expression\Unary\MinusUnary'],
        ];
    }

    public function getBinaryOperators() {
        return 
        [
            '||'  => ['precedence' => 10, 'class' => '\Tmpl\Node\Expression\Binary\OrBinary', 'assoc' => \Tmpl\ExpressionParser::LEFT_ASSOC],
            '&&'  => ['precedence' => 15, 'class' => '\Tmpl\Node\Expression\Binary\AndBinary', 'assoc' => \Tmpl\ExpressionParser::LEFT_ASSOC],
            '|'   => ['precedence' => 16, 'class' => '\Tmpl\Node\Expression\Binary\BitwiseOrBinary', 'assoc' => \Tmpl\ExpressionParser::LEFT_ASSOC],
            '^'   => ['precedence' => 17, 'class' => '\Tmpl\Node\Expression\Binary\BitwiseXorBinary', 'assoc' => \Tmpl\ExpressionParser::LEFT_ASSOC],
            '&'   => ['precedence' => 18, 'class' => '\Tmpl\Node\Expression\Binary\BitwiseAndBinary', 'assoc' => \Tmpl\ExpressionParser::LEFT_ASSOC],
            '===' => ['precedence' => 20, 'class' => '\Tmpl\Node\Expression\Binary\EqualsStrictBinary', 'assoc' => \Tmpl\ExpressionParser::LEFT_ASSOC],
            '!==' => ['precedence' => 20, 'class' => '\Tmpl\Node\Expression\Binary\NotEqualsStrictBinary', 'assoc' => \Tmpl\ExpressionParser::LEFT_ASSOC],
            '=='  => ['precedence' => 20, 'class' => '\Tmpl\Node\Expression\Binary\EqualsBinary', 'assoc' => \Tmpl\ExpressionParser::LEFT_ASSOC],
            '!='  => ['precedence' => 20, 'class' => '\Tmpl\Node\Expression\Binary\NotEqualsBinary', 'assoc' => \Tmpl\ExpressionParser::LEFT_ASSOC],
            '>='  => ['precedence' => 20, 'class' => '\Tmpl\Node\Expression\Binary\GreaterEqualBinary', 'assoc' => \Tmpl\ExpressionParser::LEFT_ASSOC],
            '>'   => ['precedence' => 20, 'class' => '\Tmpl\Node\Expression\Binary\GreaterBinary', 'assoc' => \Tmpl\ExpressionParser::LEFT_ASSOC],
            '<='  => ['precedence' => 20, 'class' => '\Tmpl\Node\Expression\Binary\LessEqualBinary', 'assoc' => \Tmpl\ExpressionParser::LEFT_ASSOC],
            '<'   => ['precedence' => 20, 'class' => '\Tmpl\Node\Expression\Binary\LessBinary', 'assoc' => \Tmpl\ExpressionParser::LEFT_ASSOC],
            '-'   => ['precedence' => 30, 'class' => '\Tmpl\Node\Expression\Binary\MinusBinary', 'assoc' => \Tmpl\ExpressionParser::LEFT_ASSOC],
            '+'   => ['precedence' => 30, 'class' => '\Tmpl\Node\Expression\Binary\PlusBinary', 'assoc' => \Tmpl\ExpressionParser::LEFT_ASSOC],
            '*'   => ['precedence' => 60, 'class' => '\Tmpl\Node\Expression\Binary\MulBinary', 'assoc' => \Tmpl\ExpressionParser::LEFT_ASSOC],
            '/'   => ['precedence' => 60, 'class' => '\Tmpl\Node\Expression\Binary\DivBinary', 'assoc' => \Tmpl\ExpressionParser::LEFT_ASSOC],
            '//'  => ['precedence' => 60, 'class' => '\Tmpl\Node\Expression\Binary\DivFloorBinary', 'assoc' => \Tmpl\ExpressionParser::LEFT_ASSOC],
            '%'   => ['precedence' => 60, 'class' => '\Tmpl\Node\Expression\Binary\ModBinary', 'assoc' => \Tmpl\ExpressionParser::LEFT_ASSOC],
            '**'  => ['precedence' => 200, 'class' => '\Tmpl\Node\Expression\Binary\PowBinary', 'assoc' => \Tmpl\ExpressionParser::RIGHT_ASSOC],
            //''    => ['precedence' => , 'class' => '\Tmpl\Node\Expression\Binary\', 'assoc' => \Tmpl\ExpressionParser::],
        ];
    }
}