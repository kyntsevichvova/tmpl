<?php

namespace Tmpl;

use \Tmpl\Token\Token;

class Lexer {
    const STATE_DATA       = 0;
    const STATE_COMMENT    = 1;
    const STATE_OUTPUT     = 2;
    const STATE_BLOCK      = 3;
    const STATE_EXPRESSION = 4;
        
    private $env;
    private $options;
    
    private $whitespaces;
    private $operators;
    private $numbers;
    private $quotes;
    private $punctuation;

    private $stream;
    private $source;
    private $code;

    private $cursor;
    private $end;

    private $state;
    private $states;

    public function __construct(Environment $env, $options = []) {
        $this->env = $env;
        $this->options = array_merge([
            'tag_comment' => ['{#', '#}'],
            'tag_output'  => ['{{', '}}'],
            'tag_block'   => ['{%', '%}'],
        ], $options);

        $this->whitespaces = [
            " ", "\r", "\n", "\t", "\v"
        ];

        $this->operators = [
            "++", "--",
            "+", "-", 
            "**", "//", "*", "/", "%",
            "!==", "===", "!=", "==", ">=", "<=", ">", "<",
            "!", "&&", "||",
            "&", "^", "|",
            "="
        ];
        
        $this->numbers = [
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
        ];

        $this->quotes = [
            "'", '"'
        ];

        $this->punctuation = [
            "(", ")", "{", "}", "[", "]", ".", ",", "@"
        ];
    }

    public function tokenize(Source $source) {
        $this->stream = new \Tmpl\Token\TokenStream();
        $this->source = $source;
        $this->code = $source->getCode();

        $this->cursor = 0;
        $this->end = strlen($this->code);

        $this->pushState(self::STATE_DATA);

        while ($this->cursor < $this->end) {
            switch ($this->state) {
                case self::STATE_DATA:
                    $this->lexData();
                    break;
                case self::STATE_OUTPUT:
                    $this->lexOutput();
                    break;
                case self::STATE_BLOCK:
                    $this->lexBlock();
                    break;
            }
        }
        return $this->stream;
    }

    private function lexData() {
        $buffer = "";
        while ($this->cursor < $this->end) {
            if ($this->matchesOne($this->options['tag_comment'][0])) {
                $this->pushToken(Token::DATA, $buffer);
                $buffer = "";
                $this->pushState(self::STATE_COMMENT);
                $this->lexComment();
            } elseif ($this->matchesOne($this->options['tag_output'][0])) {
                $this->pushToken(Token::DATA, $buffer);
                $buffer = "";
                $this->pushState(self::STATE_OUTPUT);
                $this->lexOutput();
            } elseif ($this->matchesOne($this->options['tag_block'][0])) {
                $this->pushToken(Token::DATA, $buffer);
                $buffer = "";
                $this->pushState(self::STATE_BLOCK);
                $this->lexBlock();
            } else {
                $buffer .= $this->getCurrent();
                $this->cursor++;
            }
        }
        if (strlen($buffer) > 0) {
            $this->pushToken(Token::DATA, $buffer);
        }
        $this->popState();
    }

    private function lexComment() {
        $this->pushToken(Token::COMMENT_START, $this->options['tag_comment'][0]);
        $this->cursor += strlen($this->options['tag_comment'][0]);

        $buffer = "";
        while ($this->cursor < $this->end) {
            if ($this->matchesOne($this->options['tag_comment'][1])) {
                if (strlen($buffer) > 0) {
                    $this->pushToken(Token::DATA, $buffer);
                    $buffer = "";
                }
                break;
            } else {
                $buffer .= $this->getCurrent();
                $this->cursor++;
            }
        }

        $this->pushToken(Token::COMMENT_END, $this->options['tag_comment'][1]);
        $this->cursor += strlen($this->options['tag_comment'][1]);
        $this->popState();
    }

    private function lexOutput() {
        $this->pushToken(Token::OUTPUT_START, $this->options['tag_output'][0]);
        $this->cursor += strlen($this->options['tag_output'][0]);

        $this->pushState(self::STATE_EXPRESSION);
        $this->lexExpression();

        $this->pushToken(Token::OUTPUT_END, $this->options['tag_output'][1]);
        $this->cursor += strlen($this->options['tag_output'][1]);
        $this->popState();
    }

    private function lexBlock() {
        $this->pushToken(Token::BLOCK_START, $this->options['tag_block'][0]);
        $this->cursor += strlen($this->options['tag_block'][0]);
        
        $this->pushState(self::STATE_EXPRESSION);
        $this->lexExpression();

        $this->pushToken(Token::BLOCK_END, $this->options['tag_block'][1]);
        $this->cursor += strlen($this->options['tag_block'][1]);
        $this->popState();
    }

    private function lexExpression() {
        $buffer = "";
        while ($this->cursor < $this->end) {
            if ($this->matchesOne($this->options['tag_output'][1])) {
                $this->pushToken(Token::NAME, $buffer);
                $buffer = "";
                break;
            } elseif ($this->matchesOne($this->options['tag_block'][1])) {
                $this->pushToken(Token::NAME, $buffer);
                $buffer = "";
                break;
            } elseif ($this->matches($this->whitespaces) != -1) {
                $this->pushToken(Token::NAME, $buffer);
                $buffer = "";
                $this->lexWhitespace();
            } elseif ($this->matches($this->operators) != -1) {
                $this->pushToken(Token::NAME, $buffer);
                $buffer = "";
                $this->lexOperator();
            } elseif ($this->matches($this->numbers) != -1) {
                $this->pushToken(Token::NAME, $buffer);
                $buffer = "";
                $this->lexNumber();
            } elseif ($this->matches($this->quotes) != -1) {
                $this->pushToken(Token::NAME, $buffer);
                $buffer = "";
                $this->lexString();
            } elseif ($this->matches($this->punctuation) != -1) {
                $this->pushToken(Token::NAME, $buffer);
                $buffer = "";
                $this->lexPunctuation();
            } else {
                $buffer .= $this->getCurrent();
                $this->cursor++;
            }
        }
        if (strlen($buffer) > 0) {
            $this->pushToken(Token::NAME, $buffer);
        }
        $this->popState();
    }

    private function lexWhitespace() {
        $buffer = "";
        while ($this->cursor < $this->end && $this->matches($this->whitespaces) != -1) {
            $buffer .= $this->getCurrent();
            $this->cursor++;
        }
        if (strlen($buffer) > 0 && $this->state !== self::STATE_EXPRESSION) {
            $this->pushToken(Token::WHITESPACE, $buffer);
        }
    }

    private function lexOperator() {
        $operator = $this->operators[$this->matches($this->operators)];
        $this->pushToken(Token::OPERATOR, $operator);
        $this->cursor += strlen($operator);
    }

    private function lexNumber() {
        $buffer = "";
        while ($this->cursor < $this->end && $this->matches($this->numbers) != -1) {
            $buffer .= $this->getCurrent();
            $this->cursor++;
        }
        if (strlen($buffer) > 0) {
            $this->pushToken(Token::NUMBER_LIT, $buffer);
        }
    }

    private function lexString() {
        $buffer = "";
        $quote = $this->quotes[$this->matches($this->quotes)];
        $this->cursor++;
        $escaped = false;
        while ($this->cursor < $this->end) {
            if ($this->getCurrent() === "\\") {
                if ($escaped) {
                    $buffer .= "\\";
                }
                $escaped = !$escaped;
            } elseif ($this->getCurrent() === $quote) {
                if ($escaped) {
                    $buffer .= $quote;
                    $escaped = false;
                } else {
                    $this->cursor++;
                    break;
                }
            } else {
                $buffer .= $this->getCurrent();
            }
            $this->cursor++;
        }
        $this->pushToken(Token::STRING_LIT, $quote . $buffer . $quote);
    }

    private function lexPunctuation() {
        $punctuation = $this->punctuation[$this->matches($this->punctuation)];
        $this->pushToken(Token::PUNCTUATION, $punctuation);
        $this->cursor += strlen($punctuation);
    }

    private function getCurrent() {
        return $this->code{$this->cursor};
    }

    private function pushState($state) {
        $this->states[] = $this->state;
        $this->state = $state;
    }

    private function popState() {
        if (count($this->states) === 0) {
            // Error
        }

        $this->state = array_pop($this->states);
    }

    private function pushToken($tokenType, $value) {
        if (strlen($value) === 0) {
            return;
        } 
        $token = new Token($tokenType, $value);
        $this->stream->push($token);
    }

    private function matches(array $values) {
        foreach ($values as $i => $value) {
            if ($this->matchesOne($value)) {
                return $i;
            }
        }

        return -1;
    }

    private function matchesOne($value) {
        if ($this->end - $this->cursor < strlen($value)) {
            return false;
        }

        return strcmp(substr($this->code, $this->cursor, strlen($value)), $value) === 0;
    }
}