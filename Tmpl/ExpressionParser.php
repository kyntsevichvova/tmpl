<?php

namespace Tmpl;

use Tmpl\Token\Token;
use Tmpl\Token\TokenStream;

use Tmpl\Node\Expression\ConstantExpression;
use Tmpl\Node\Expression\NameExpression;
use Tmpl\Node\Expression\ArrayExpression;
use Tmpl\Node\Expression\FunctionExpression;
use Tmpl\Node\Expression\MemberAccessExpression;
use Tmpl\Node\Expression\FilterExpression;
use Tmpl\Node\Expression\AssignNameExpression;

class ExpressionParser {
    const RIGHT_ASSOC = 0;
    const LEFT_ASSOC = 1;

    private $env;
    private $unary;
    private $binary;
    private $functions;
    private $filters;

    public function __construct($env) {
        $this->env = $env;
        $this->unary = $env->getUnary();
        $this->binary = $env->getBinary();
        $this->functions = $env->getFunctions();
        $this->functions = $env->getFilters();
    }

    public static function cutStream(TokenStream $originalStream) {
        $stream = new TokenStream();

        while (!$originalStream->empty()) {
            switch ($originalStream->peek()->getType()) {
                case Token::OUTPUT_END:
                case Token::BLOCK_END:
                    break;
                case Token::WHITESPACE:
                    $originalStream->pop();
                    break;
                default:
                    $stream->push($originalStream->pop());
            }
        }

        return $stream;
    }

    public function parse(TokenStream $originalStream) {
        //$stream = self::cutStream($originalStream);

        //return parseExpression($stream);
        $node = $this->parseExpression($originalStream);
        return $node;
    }

    public function parseExpression(TokenStream $stream, $precedence = 0) {
        $lookahead = $stream->peek();
        $node = $this->getPrimary($stream);

        while ($this->isBinary($lookahead) && $this->binary[$lookahead->getValue()]['precedence'] >= $precedence) {
            $op = $this->binary[$lookahead->getValue()];
            $stream->pop();

            $expr = $this->parseExpression($stream, $op['precedence'] + $op['assoc']);
            $class = $op['class'];
            $node = new $class($node, $expr);

            $lookahead = $stream->peek();
        }

        return $node;
    }

    public function getPrimary(TokenStream $stream) {
        $token = $stream->peek();
        
        if ($this->isUnary($token)) {
            $op = $this->unary[$token->getValue()];
            $stream->pop();
            $expr = $this->parseExpression($stream, $op['precedence']);
            $class = $op['class'];

            return $this->parsePostfixExpression($stream, new $class($expr));
        } elseif ($token->test(Token::PUNCTUATION, '(')) {
            $stream->pop();
            $expr = $this->parseExpression($stream);
            $stream->expect(Token::PUNCTUATION, ')');

            return $this->parsePostfixExpression($stream, $expr);
        }

        return $this->parsePrimaryExpression($stream);
    }

    private function parsePrimaryExpression(TokenStream $stream) {
        $token = $stream->peek();
        switch ($token->getType()) {
            case Token::NAME:
                $stream->pop();
                switch ($token->getValue()) {
                    case 'true':
                        $node = new ConstantExpression(true);
                        break;
                    case 'false':
                        $node = new ConstantExpression(false);
                        break;
                    case 'null':
                        $node = new ConstantExpression(null);
                        break;
                    default:
                        if ($stream->peek()->test(Token::PUNCTUATION, '(')) {
                            $node = $this->getFunctionNode($stream, $token->getValue());
                        } else {
                            $node = new NameExpression($token->getValue());
                        }
                }
                break;
            
            case Token::NUMBER_LIT:
            case Token::STRING_LIT:
                $stream->pop();
                $node = new ConstantExpression($token->getValue());
                break;

            // Unary??

            default:
                if ($token->test(Token::PUNCTUATION, '[')) {
                    $node = $this->parseArrayExpression($stream);
                } else {
                    // Error
                }
        }
        return $this->parsePostfixExpression($stream, $node);
    }

    private function parsePostfixExpression(TokenStream $stream, $node) {
        while (true) {
            $token = $stream->peek();
            if ($token->getValue() == '.' || $token->getValue() == '[') {
                $node = $this->parseMemberAccessExpression($stream, $node);
            } elseif ($token->getValue() == '|') {
                $node = $this->parseFilterExpression($node);
            } else {
                break;
            }
        }

        return $node;
    }

    private function parseArrayExpression(TokenStream $stream) {
        $stream->expect(Token::PUNCTUATION, '[');

        $node = new ArrayExpression();
        $node->addChild($this->parseExpression($stream));
        while (!$stream->test(Token::PUNCTUATION, ']')) {
            $stream->expect(Token::PUNCTUATION, ',');
            $node->addChild($this->parseExpression($stream));
        }

        $stream->expect(Token::PUNCTUATION, ']');
        return $node;
    }

    private function getFunctionNode(TokenStream $stream, $name) {
        $args = $this->parseArguments($stream);

        if (!isset($this->functions[$name])) {
            // Error
        }
        
        return new FunctionExpression($this->functions[$name], $args);
    }
    
    private function parseMemberAccessExpression(TokenStream $stream, $node) {
        $token = $stream->pop();
        if ($token->getValue() == '.') {
            $token = $stream->pop();
            $member = new ConstantExpression($token->getValue());
        } else {
            $member = $this->parseExpression($stream);
            $stream->expect(Token::PUNCTUATION, ']');
        }
        return new MemberAccessExpression($node, $member);
    }

    private function parseFilterExpression(TokenStream $stream, $node) {
        while (true) {
            $token = $stream->expect(Token::NAME);

            $name = $token->getValue();

            $node = $this->getFilterNode($stream, $name, $node);

            if (!$stream->test(Token::PUNCTUATION, '@')) {
                break;
            }

            $stream->pop();
        }

        return $node;
    }

    private function getFilterNode(TokenStream $stream, $name, $node) {
        if (!isset($this->filters)) {
            // Error
        }

        if (!$stream->test(Token::PUNCTUATION, '(')) {
            $args = null;
        } else {
            $args = $this->parseArguments($stream);
        }
        
        return new FilterExpression($this->filters[$name], $node, $args);
    }

    private function parseArguments(TokenStream $stream) {
        $args = [];
        $stream->expect(Token::PUNCTUATION, '(');

        while (!$stream->test(Token::PUNCTUATION, ')')) {
            if (!empty($args)) {
                $stream->expect(Token::PUNCTUATION, ',');
            }

            $value = $this->parseExpression($stream);
            $args[] = $value;
        }

        $stream->expect(Token::PUNCTUATION, ')');
        return $args;
    }

    public function parseAssignmentExpression(TokenStream $stream) {
        $token = $stream->expect(Token::NAME);
        $value = $token->getValue();
        if (\in_array(strtolower($value), ['true', 'false', 'none', 'null'])) {
            // Error
        }
        $target = new AssignNameExpression($value);

        return $target;
    }

    private function isUnary(Token $token) {
        return $token->test(Token::OPERATOR) && isset($this->unary[$token->getValue()]);
    }

    private function isBinary(Token $token) {
        return $token->test(Token::OPERATOR) && isset($this->binary[$token->getValue()]);
    }

}